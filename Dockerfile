# Stage 1: Build the React application
FROM node:18 as build-stage

WORKDIR /app

COPY ./client/package*.json /app/

RUN npm install

COPY ./client/ /app/

RUN npm run build

# Stage 2: Build the Node application
FROM node:18

WORKDIR /app

COPY ./source/package*.json /app/

RUN npm install

COPY ./source/ /app/

# Copy the React build from the previous stage
COPY --from=build-stage /app/build /app/public

EXPOSE 3000

CMD ["node", "index.js"]
