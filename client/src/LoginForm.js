import React, { useState } from 'react';
import './LoginForm.css';	


const LoginForm = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  
const handleSubmit = async (event) => {
  event.preventDefault();
  try {
    const response = await fetch('http://172.22.107.32:3001/authenticate', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ username, password }),
    });
    
    if (!response.ok) {
      throw new Error('Login failed');
    }
    const data = await response.json();
    console.log('Login successful:', data);
    // Handle successful login here (e.g., redirect, store tokens)
  } catch (error) {
    console.error('Login error:', error);
    // Handle errors (e.g., show error message)
  }
};

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor="username">Username:</label>
        <input
          type="text"
          id="username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
      </div>
      <div>
        <label htmlFor="password">Password:</label>
        <input
          type="password"
          id="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </div>
      <button type="submit">Login</button>
    </form>
  );
  

};




export default LoginForm;
