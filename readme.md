# Docker
- **Run**: Execute `docker-compose up -d` in the base directory.
- **Development mode**: First, run `docker-compose stop app`. Then run the client and server independently until Docker is configured to work with React. IP addresses are manually in use until Docker is fully functional.

# Client
- **Run**: Execute `npm start` in the `/client` directory.

# Server
- **Run**: Execute `node index.js` in the `/source` directory.


